<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
    * @Route("/", name="homepage")
    */

    public function indexAction(EntityManagerInterface $entityManager): Response
    {
        return $this->render('index.html.twig', [ ]);
    }

    public function loginAction(Request $request)
    {
        return $this->render('login.html.twig', array(
        ));
    }
}