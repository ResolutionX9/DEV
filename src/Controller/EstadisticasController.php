<?php

namespace App\Controller;

use App\Entity\Estadisticas;
use App\Form\EstadisticasType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/estadisticas")
 */
class EstadisticasController extends AbstractController
{
    /**
     * @Route("/", name="app_estadisticas_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $estadisticas = $entityManager
            ->getRepository(Estadisticas::class)
            ->findAll();

        return $this->render('estadisticas/index.html.twig', [
            'estadisticas' => $estadisticas,
        ]);
    }

    /**
     * @Route("/new", name="app_estadisticas_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $estadistica = new Estadisticas();
        $form = $this->createForm(EstadisticasType::class, $estadistica);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($estadistica);
            $entityManager->flush();

            return $this->redirectToRoute('app_estadisticas_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('estadisticas/new.html.twig', [
            'estadistica' => $estadistica,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_estadisticas_show", methods={"GET"})
     */
    public function show(Estadisticas $estadistica): Response
    {
        return $this->render('estadisticas/show.html.twig', [
            'estadistica' => $estadistica,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_estadisticas_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Estadisticas $estadistica, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(EstadisticasType::class, $estadistica);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_estadisticas_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('estadisticas/edit.html.twig', [
            'estadistica' => $estadistica,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_estadisticas_delete", methods={"POST"})
     */
    public function delete(Request $request, Estadisticas $estadistica, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$estadistica->getId(), $request->request->get('_token'))) {
            $entityManager->remove($estadistica);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_estadisticas_index', [], Response::HTTP_SEE_OTHER);
    }
}
