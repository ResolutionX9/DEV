<?php

namespace App\Form;

use App\Entity\Estadisticas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstadisticasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lanzamientos')
            ->add('goles')
            ->add('perdidas')
            ->add('recuperaciones')
            ->add('lanzamientosp')
            ->add('paradas')
            ->add('golesrecibidos')
            ->add('idJugador')
            ->add('idPartido')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Estadisticas::class,
        ]);
    }
}
