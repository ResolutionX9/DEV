<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partidos
 *
 * @ORM\Table(name="Partidos", indexes={@ORM\Index(name="fk_idEquipoPartido", columns={"ID_Equipo"})})
 * @ORM\Entity
 */
class Partidos
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Rival", type="string", length=255, nullable=false)
     */
    private $rival;

    /**
     * @var string
     *
     * @ORM\Column(name="Resultado", type="string", length=7, nullable=false)
     */
    private $resultado;

    /**
     * @var int
     *
     * @ORM\Column(name="Jornada", type="integer", nullable=false)
     */
    private $jornada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var \Equipos
     *
     * @ORM\ManyToOne(targetEntity="Equipos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_Equipo", referencedColumnName="ID")
     * })
     */
    private $idEquipo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRival(): ?string
    {
        return $this->rival;
    }

    public function setRival(string $rival): self
    {
        $this->rival = $rival;

        return $this;
    }

    public function getResultado(): ?string
    {
        return $this->resultado;
    }

    public function setResultado(string $resultado): self
    {
        $this->resultado = $resultado;

        return $this;
    }

    public function getJornada(): ?int
    {
        return $this->jornada;
    }

    public function setJornada(int $jornada): self
    {
        $this->jornada = $jornada;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getIdEquipo(): ?Equipos
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipos $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }


}
