<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estadisticas
 *
 * @ORM\Table(name="Estadisticas", indexes={@ORM\Index(name="fk_idJugador", columns={"ID_Jugador"}), @ORM\Index(name="fk_idPartido", columns={"ID_Partido"})})
 * @ORM\Entity
 */
class Estadisticas
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="Lanzamientos", type="integer", nullable=false)
     */
    private $lanzamientos;

    /**
     * @var int
     *
     * @ORM\Column(name="Goles", type="integer", nullable=false)
     */
    private $goles;

    /**
     * @var int
     *
     * @ORM\Column(name="Perdidas", type="integer", nullable=false)
     */
    private $perdidas;

    /**
     * @var int
     *
     * @ORM\Column(name="Recuperaciones", type="integer", nullable=false)
     */
    private $recuperaciones;

    /**
     * @var int
     *
     * @ORM\Column(name="LanzamientosP", type="integer", nullable=false)
     */
    private $lanzamientosp;

    /**
     * @var int
     *
     * @ORM\Column(name="Paradas", type="integer", nullable=false)
     */
    private $paradas;

    /**
     * @var int
     *
     * @ORM\Column(name="GolesRecibidos", type="integer", nullable=false)
     */
    private $golesrecibidos;

    /**
     * @var \Jugadores
     *
     * @ORM\ManyToOne(targetEntity="Jugadores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_Jugador", referencedColumnName="ID")
     * })
     */
    private $idJugador;

    /**
     * @var \Partidos
     *
     * @ORM\ManyToOne(targetEntity="Partidos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_Partido", referencedColumnName="ID")
     * })
     */
    private $idPartido;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanzamientos(): ?int
    {
        return $this->lanzamientos;
    }

    public function setLanzamientos(int $lanzamientos): self
    {
        $this->lanzamientos = $lanzamientos;

        return $this;
    }

    public function getGoles(): ?int
    {
        return $this->goles;
    }

    public function setGoles(int $goles): self
    {
        $this->goles = $goles;

        return $this;
    }

    public function getPerdidas(): ?int
    {
        return $this->perdidas;
    }

    public function setPerdidas(int $perdidas): self
    {
        $this->perdidas = $perdidas;

        return $this;
    }

    public function getRecuperaciones(): ?int
    {
        return $this->recuperaciones;
    }

    public function setRecuperaciones(int $recuperaciones): self
    {
        $this->recuperaciones = $recuperaciones;

        return $this;
    }

    public function getLanzamientosp(): ?int
    {
        return $this->lanzamientosp;
    }

    public function setLanzamientosp(int $lanzamientosp): self
    {
        $this->lanzamientosp = $lanzamientosp;

        return $this;
    }

    public function getParadas(): ?int
    {
        return $this->paradas;
    }

    public function setParadas(int $paradas): self
    {
        $this->paradas = $paradas;

        return $this;
    }

    public function getGolesrecibidos(): ?int
    {
        return $this->golesrecibidos;
    }

    public function setGolesrecibidos(int $golesrecibidos): self
    {
        $this->golesrecibidos = $golesrecibidos;

        return $this;
    }

    public function getIdJugador(): ?Jugadores
    {
        return $this->idJugador;
    }

    public function setIdJugador(?Jugadores $idJugador): self
    {
        $this->idJugador = $idJugador;

        return $this;
    }

    public function getIdPartido(): ?Partidos
    {
        return $this->idPartido;
    }

    public function setIdPartido(?Partidos $idPartido): self
    {
        $this->idPartido = $idPartido;

        return $this;
    }


}
